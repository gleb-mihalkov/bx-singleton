<?php
namespace Bx\Singleton;
use Bx\Assert\Assert;

/**
 * Реализация синглтона.
 */
trait Singleton
{
    /**
     * Создает и возвращает экземпляр синглтона. Метод может быть перекрыт классом-наследником.
     * @return self Экземпляр синглтона.
     */
    protected static function createInstance()
    {
        return new static();
    }

    /**
     * Коллекция экземпляров синглтона, сгруппированная по именам классов, которым
     * они соответствуют.
     * @var array
     */
    protected static $singletons = [];

    /**
     * Получает экземпляр синглтона.
     * @return self Экземпляр.
     */
    public static function getInstance() : self
    {
        $class = static::class;
        $instance = self::$singletons[$class] ?? null;

        if (!$instance)
        {
            $instance = static::createInstance();
            Assert::isInstanceOf($instance, $class);

            self::$singletons[$class] = $instance;
        }

        return $instance;
    }
}
